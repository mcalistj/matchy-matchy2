package com.shapes.matchymatchy;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

public class Settings extends Activity{
	
	ToggleButton B4;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
	
		B4 =(ToggleButton)findViewById(R.id.toggleMusic); //Toggle Music Button
		B4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			
			
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				
				
				

				if(isChecked && Menu.musicStart==false){
					
					Menu.mainMusic = MediaPlayer.create(Settings.this, R.raw.mainmusic);
					Menu.mainMusic.start();
					Menu.musicStart=true;
				}
				
				else if(isChecked==false){
					
					Menu.mainMusic.stop();
					Menu.musicStart=false;

				}
				
			}
		});

		
		
		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//finish();
	}
	
	protected void onResume(){
		
		super.onResume();
		
		if(Menu.musicStart==true){
			
			B4.setChecked(true);
		}
		
	}
	
}
