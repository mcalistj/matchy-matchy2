package com.shapes.matchymatchy;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;

public class Splash extends Activity{

	MediaPlayer ourSong;
	boolean started = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		ourSong = MediaPlayer.create(Splash.this, R.raw.flute);
		ourSong.start();
		Thread timer = new Thread(){
			public void run(){
				try{
					sleep(22000);
				} catch (InterruptedException e){
					e.printStackTrace();
				}finally{
					if(!started){
						Intent openMainMenu = new Intent("android.intent.action.Menu");
						startActivity(openMainMenu); //<!--comment-->
						started=true;
					}
				}
			}
		};
		timer.start();
	}
	
	public void skip(View v){
		if(!started){
			Intent openMainMenu = new Intent("android.intent.action.Menu");
			startActivity(openMainMenu); //<!--comment-->
			started=true;
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		ourSong.release();
		finish();
	}
	
}
