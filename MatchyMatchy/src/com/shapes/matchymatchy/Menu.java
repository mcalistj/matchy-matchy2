package com.shapes.matchymatchy;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;


public class Menu extends Activity{

	public boolean a = false;
	public static boolean musicStart = false;
	public static MediaPlayer mainMusic;
	public static int LastScore;
	public String sdcard = Environment.getExternalStorageDirectory()+"/Highscore";
	public File file = new File(sdcard,"sample.txt");
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);
	
		
		
	Button B1 = (Button)findViewById(R.id.StartButton); //Defining B1 equal to button object B1 in .xml
	B1.setOnClickListener(new View.OnClickListener() { //B1 on click
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
		

			Intent openMainActivity = new Intent("android.intent.action.MAINACTIVITY");
			startActivity(openMainActivity);		
			
		}
	});
	
	Button B2 =(Button)findViewById(R.id.RulesButton);
	B2.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			setContentView(R.layout.rules);
			a = true;
			
		}
	});
	
	Button B3 =(Button)findViewById(R.id.SettingsButton);
	B3.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			Intent openSettings = new Intent("android.intent.action.Settings");
			startActivity(openSettings); 		
						a = true;
			
		}
	});
	
	Button B4 =(Button)findViewById(R.id.ScoreButton);
	B4.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			Intent openScores = new Intent("android.intent.action.Scores");
			startActivity(openScores); 		
						a = true;
			
		}
	});
	
try{ //Reads in previous highscore if any from file
	@SuppressWarnings("resource")
	BufferedReader br = new BufferedReader(new FileReader(file));
    String line = null;
    line = br.readLine();
    if(line!=null){
        LastScore = Integer.parseInt(line);
	    System.out.println("Success!");

    }
    
       
	}catch (IOException e){
	    System.err.println(e);

	}
 
 

	
}
	
    @Override
    protected void onResume() {
        // Ideally a game should implement onResume() and onPause()
        // to take appropriate action when the activity looses focus
        super.onResume();
//        if(musicStart==true){
//        	
//        	mainMusic.start();
//        	
//        }

    }

    @Override
    protected void onPause() {
        // Ideally a game should implement onResume() and onPause()
        // to take appropriate action when the activity looses focus
        super.onPause();
        
//        if(musicStart==true){
//        mainMusic.stop();
//   		}
   
    }

    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode == KeyEvent.KEYCODE_BACK && a == true) {
        		Intent openMainMenu = new Intent("android.intent.action.Menu");
        		startActivity(openMainMenu); //<!--comment-->         
                finish();
                a = false;
                return true;
        }
        
        else
        	System.exit(0);
        	return false;
    }


	
	
}
