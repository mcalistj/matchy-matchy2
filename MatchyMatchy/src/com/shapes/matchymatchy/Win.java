package com.shapes.matchymatchy;


import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;



public class Win extends Activity { //Test

	
	
	private TextView scoreTV;
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			Intent intent = getIntent();
			setContentView(R.layout.win);
     		int score = -1;
			if(intent.getStringExtra(MainActivity.EXTRA_MESSAGE) != null){
				String msg = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
		     	score = Integer.parseInt(msg);
	     	}
			
	        scoreTV=(TextView)findViewById(R.id.yourScore);
	        String text = "Score: "+score;
	        scoreTV.setText(text);
			
		}
		
		public void goMenu(View v){
			Intent score = new Intent("android.intent.action.Scores");
			startActivity(score);	
		}
}
