package com.shapes.matchymatchy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Scores extends Activity{
	
	public String sdcard = Environment.getExternalStorageDirectory()+"/Highscore";
	public File file = new File(sdcard,"sample.txt");
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scores);
		
		String sdcard = Environment.getExternalStorageDirectory()+"/Highscore";

		
		
		//Get the text file
		File file = new File(sdcard,"sample.txt");

		//Read text from file
		StringBuilder text = new StringBuilder();

		try {
		    @SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new FileReader(file));
		    String line;

		    while ((line = br.readLine()) != null) {
		        text.append(line);
		        text.append('\n');
		    }
		}
		catch (IOException e) {
		    System.err.println(e);
		}finally{
			
		}
		
		//Find the view by its id
		TextView tv = (TextView)findViewById(R.id.text_view);

		//Set the text
		tv.setText(text);
		
		Button B1 = (Button)findViewById(R.id.resetScore); //Defining B1 equal to button object B1 in .xml
		B1.setOnClickListener(new View.OnClickListener() { //B1 on click
			
			@Override
			public void onClick(View arg0) {

				generateNoteOnSD("sample.txt", "0"); //Generates blank samples.txt file in "Highscore" folder 	
				
			}
		});
		
		
	
	}
	
    @Override
    protected void onResume() {
    	
        super.onResume();


    }

    @Override
    protected void onPause() {
  
        super.onPause();
        

   
    }
	
	  public void generateNoteOnSD(String sFileName, String sBody){
	        try
	        {
	            File root = new File(Environment.getExternalStorageDirectory(), "HighScore");
	            if (!root.exists()) {
	                root.mkdirs();
	            }
	            File gpxfile = new File(root, sFileName);
	            FileWriter writer = new FileWriter(gpxfile);
	            writer.append(sBody);
	            writer.flush();
	            writer.close();
	            Menu.LastScore=999;
	            Toast.makeText(this, "HighScore Reset", Toast.LENGTH_SHORT).show();
	        }
	        catch(IOException e)
	        {
	             e.printStackTrace();
	       }
	       }  
	
	
}