package com.shapes.matchymatchy;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static java.lang.Math.abs;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint({ "InlinedApi", "NewApi" })
public class MainActivity extends Activity {

	SoundPool sp;
	int tick = 0;
		
	private GLSurfaceView mGLSurfaceView;
	
	private int number; //Variables for timer
	private TextView textfield, levelTV;
	
	public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
	public int levelNo;
	
	
	private Handler handler;
//	private Random generator;
	Random x = new Random();//used to generate Cube, Pyramid, Sphere at random
	public int r=x.nextInt(2)+1;
	
	String extStore = Environment.getExternalStorageDirectory()+"/Highscore"+"/sample.txt"; //path to file that stores winning score

    private SensorManager mSensorManager;
    private MyRenderer mRenderer;
    public String myTag="myInfo";
    public int zZoom=0;
    public int yZoom=0;
    long startTime= System.nanoTime();
    float mul=10;
    public float[] vals;
    public boolean Running = true;
    public boolean pauseTimer = false;
	public boolean Win = false;
	List<ImageView> images = new ArrayList<ImageView>();
	int[] imageSrcs={R.drawable.tri, R.drawable.ci, R.drawable.sq};
	int[] rIDs = {R.id.imageView1, R.id.imageView2, R.id.imageView3};
	int[] randIndexes=new int[3];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //Setting View
		setContentView(R.layout.activity_main);
		
		sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
		tick = sp.load(this, R.raw.tick , 1);
		
    	Log.d(myTag, "Created!");

        // Get an instance of the SensorManager
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.Rl);
				
        textfield=(TextView)findViewById(R.id.TVTimer);
        
        // Get level from the intent
     	levelTV=(TextView)findViewById(R.id.levelNum);
     	Intent intent = getIntent();
     	if(intent.getStringExtra(MainActivity.EXTRA_MESSAGE) != null){
     		String msg = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
     		String[] parts = msg.split(",");
            String lev = parts[0];
            String ti  = parts[1];
	     	levelNo = Integer.parseInt(lev);
	     	number = Integer.parseInt(ti);
	     	Log.d(myTag, "LevelNo:"+levelNo);
     	}else{
     		levelNo=1;
     		Log.d(myTag, "LevelNo:"+levelNo);
     	}
     	levelTV.setText("Level "+levelNo);	
     	//Set Level
    	textfield=(TextView)findViewById(R.id.TVTimer);
        
        
        handler = new Handler();
       
        Runnable runnable = new Runnable(){ //Timer Function
        	
        	public void run(){
        		
        		while(Running){
        			
        			try{
        				
        				Thread.sleep(1000);}
        				
        				catch(InterruptedException e){
        					
        					e.printStackTrace();
        					
        				}
        				handler.post(new Runnable(){
        					@Override
        					
        					public void run(){
        					
        					if(pauseTimer==false){	
        						
        						number+=1;
        						
        						textfield.setText(String.valueOf(number));	
        					}
        					}	
        					
        				});
        			}
        		}
        };
        new Thread(runnable).start(); //Starts timer when main activity created
        		
        
        
        
        
        
        // Create our Preview view and set it as the content of our
        // Activity
        mRenderer = new MyRenderer();
        mGLSurfaceView = new GLSurfaceView(this);
        
        
        
        mGLSurfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0);
        
        
        mGLSurfaceView.setRenderer(mRenderer);
        
        mGLSurfaceView.getHolder().setFormat(PixelFormat.TRANSLUCENT);
        mGLSurfaceView.setZOrderOnTop(true);  

        
        rl.addView(mGLSurfaceView);
		

		
        //addContentView(iv, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));

		//addContentView(editBox, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		//addContentView(mGLSurfaceView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        
        //Make image list
		for(int i=0; i<3; i++){
			ImageView image=(ImageView) findViewById(rIDs[i]);
			images.add(image);
		}
		
		//Start shuffle
		List<Integer> dataList = new ArrayList<Integer>();
	    for (int i = 0; i < imageSrcs.length; i++) {
	    	dataList.add(i);
	    }
	    Collections.shuffle(dataList);
	    //int[] num = new int[3];
	    
	    for (int i = 0; i < 3; i++) {
	    	randIndexes[i] = dataList.get(i); 
	    	//Log.d(myTag, "Ri="+randIndexes[i]);
	    	images.get(i).setImageResource(imageSrcs[randIndexes[i]]);
	    }
        
    }
    

    @Override
    public void onBackPressed() {
        // your code.
    	Intent openMenu = new Intent("android.intent.action.Menu");
	 	startActivity(openMenu);
    	return;
    }

    @Override
    protected void onResume() {
        // Ideally a game should implement onResume() and onPause()
        // to take appropriate action when the activity looses focus
        super.onResume();
        mRenderer.start();
        mGLSurfaceView.onResume();
        pauseTimer=false;
    }

    @Override
    protected void onPause() {
        // Ideally a game should implement onResume() and onPause()
        // to take appropriate action when the activity looses focus
        super.onPause();
        mRenderer.stop();
        mGLSurfaceView.onPause();
        pauseTimer=true;
    }

    

	public void rotateRight(View v) {
		Log.d(myTag, "Right");
		int[] temp=new int[3];
		sp.play(tick, 2, 2, 0, 0, 1);
		for (int i = 0; i < 3; i++)
			temp[i]=randIndexes[(i-1)<0?2:i-1];
		
		for(int i=0; i<3; i++){
			randIndexes[i]=temp[i];
			Log.d(myTag, "pos:"+i+"="+randIndexes[i]);
			images.get(i).setImageResource(imageSrcs[randIndexes[i]]);
		}
	}
	public void rotateLeft(View v) {
		Log.d(myTag, "Left");
		sp.play(tick, 2, 2, 0, 0, 1);
		int[] temp=new int[3];

		for (int i = 0; i < 3; i++)
			temp[i]=randIndexes[(i+1)>2?0:i+1];
		
		for(int i=0; i<3; i++){
			randIndexes[i]=temp[i];
			//Log.d(myTag, "pos:"+i+"="+randIndexes[i]);
			images.get(i).setImageResource(imageSrcs[randIndexes[i]]);
		}
		

		
	}
	
	public void centerClick(View v){//test method if win/lose
		float[] values;
		values = vals.clone();


		 //stops timer when win state occurs
		
		
	if(( r == randIndexes[1]+1)&&(r==1))//check if it is a pyramid
	{
			
						//Heck of alot of IF statements due to a pyramid fitting into multiple triangles
			if ((values[0] < 0.5) && (values[0] > 0.4)) {
				if ((abs(values[1]) > 0.41) && (abs(values[1]) < 0.49)){ 
					if((abs(values[2])>0.47)&&(abs(values[2])<0.57)){
					Youwin(values);
					return;}
						 else{ 
							Youlose(values);
							return;}
							} 
					else{ 
					Youlose(values);
					return;}
				}

		
		if ((abs(values[0]) < 0.1)) {
			if ((abs(values[1]) < 0.7) && (abs(values[1]) > 0.62)){ 
				if((abs(values[2])<0.82)&&(abs(values[2])>0.71)){
				Youwin(values);
				return;}
					 else{ 
						Youlose(values);
						return;}
						} 
				else{ 
				Youlose(values);
				return;}
		}
		if ((abs(values[1]) < 0.1) && (abs(values[2])<0.1)) {
			if ((abs(values[0]) < 0.7) && (abs(values[0]) > 0.62)){ 
				Youwin(values);
				return;}
					 else{ 
						Youlose(values);
						return;}
						} 
			
		
		else{ 
		Youlose(values);
	return;
	}
	
	
	}
	
	if(( r == randIndexes[1])&&(r==2)){ //Cube
		if ((abs(values[0]) > 0.48)&&(abs(values[0])<0.52)) {
			if ((abs(values[1]) > 0.48)&&(abs(values[1])<0.52)){ 
				if ((abs(values[2]) > 0.48)&&(abs(values[2])<0.52)){
				Youwin(values);
				return;}
					 else{ 
						Youlose(values);
						return;}
						} 
				else{ 
				Youlose(values);
				return;}
		}	
		
		if ((abs(values[0]) < 0.02)) {
			if ((abs(values[1]) < 0.02)){ 
				if ((abs(values[2]) < 0.72)&&(abs(values[2])>0.685)){
				Youwin(values);
				return;}
				else	if(abs(values[2])> 0.9){
					Youwin(values);
					return;}
					 else{ 
						Youlose(values);
						return;}
						} 
				else{ 
					if ((abs(values[1]) < 0.72)&&(abs(values[1])>0.685)){ 
						if ((abs(values[2]) < 0.72)&&(abs(values[2])>0.685)){
						Youwin(values);
						return;}
							 else{ 
								Youlose(values);
								return;}
								}
					else if(abs(values[1])>0.9){
						Youwin(values);
						return;}
					
						else{ 
						Youlose(values);
						return;}}
		}	
		if ((abs(values[0]) < 0.72)&&(abs(values[0])>0.685)) {
			if ((abs(values[1]) < 0.72)&&(abs(values[1])>0.685)){ 
				if (abs(values[2])<0.02){
				Youwin(values);
				return;}
					 else{ 
						Youlose(values);
						return;}
						} 
				else{ 

					if (abs(values[2])<0.02){ 
						if (abs(values[2])<0.02){
						Youwin(values);
						return;}
							 else{ 
								Youlose(values);
								return;}
								} 
						else{ 
						Youlose(values);
						return;}
				}
		}	

	}	
	

	

Youlose(values);
		
	}
		
	public void Youwin(float[]value){
		
		Context context = getApplicationContext();
		int duration = Toast.LENGTH_SHORT;
		CharSequence text = "You Win!!!\n"/*+value[0]+"\ny="+value[1]+"\nz="+value[2]*/;
		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
		Win=true;
		
		Running=false; //stops timer when win state occurs
		
		
		final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
            	if(levelNo == 5){
        			PrintWriter out = null; //Saves Winning Time To File sample.txt TEST TEST TEST
        			
        			int score = (100-number)<=0?5:100-number;
        			
        			
        			if(Menu.LastScore<score){
        			try {
        			    out = new PrintWriter(new BufferedWriter(new FileWriter(extStore, false)));
        			    out.println(score + "\n");
        			    System.out.println("Success!");
        			    Menu.LastScore=score;
            			Log.i("myInfo", "Score:"+score);
        			}catch (IOException e) {
        			    System.err.println(e);
        			}finally{
        			    if(out != null){
        			        out.close();
        			    }
        			} 
        			}
        			Intent win = new Intent("android.intent.action.Win");
        			String message = ""+score;
        			win.putExtra(EXTRA_MESSAGE, message);
        			startActivity(win);	
        		}
        		else{
        			Intent openMainActivity = new Intent("android.intent.action.MAINACTIVITY");
        		 	levelNo++;
        		 	String message = ""+levelNo+","+number;
        		 	openMainActivity.putExtra(EXTRA_MESSAGE, message);
        		 	startActivity(openMainActivity);
        		}
            }
        }, 1000);
		
	} 



	public void Youlose(float[]value){
		
		number+=2;
		Context context = getApplicationContext();
		int duration = Toast.LENGTH_SHORT;
		CharSequence text = "2 Sec. Penalty!\nTry Again"/*+value[0]+"\ny="+value[1]+"\nz="+value[2]*/;
		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
		
	} 	
	
    class MyRenderer implements GLSurfaceView.Renderer, SensorEventListener {
        private pyramid mpyramid;
        private	 Cube mcube;
        private Sensor mRotationVectorSensor;
        private final float[] mRotationMatrix = new float[16];
 



        public MyRenderer() {
            // find the rotation-vector sensor
            mRotationVectorSensor = mSensorManager.getDefaultSensor(
                    Sensor.TYPE_ROTATION_VECTOR);

			if(r == 1)
			mpyramid = new pyramid();
			if(r==2)
			mcube = new Cube();
            // initialize the rotation matrix to identity
            mRotationMatrix[ 0] = 1;
            mRotationMatrix[ 4] = 1;
            mRotationMatrix[ 8] = 1;
            mRotationMatrix[12] = 1;
        }

        public void start() {
            // enable our sensor when the activity is resumed, ask for
            // 10 ms updates.
            mSensorManager.registerListener(this, mRotationVectorSensor, 10000);
        }

        public void stop() {
            // make sure to turn our sensor off when the activity is paused
            mSensorManager.unregisterListener(this);
        }

        public void onSensorChanged(SensorEvent event) {
            // we received a sensor event. it is a good practice to check
            // that we received the proper event
            if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
                // convert the rotation-vector to a 4x4 matrix. the matrix
                // is interpreted by Open GL as the inverse of the
                // rotation-vector, which is what we want.
            	for(int i=0, n=event.values.length; i<n; i++){
            		//Log.d(myTag, i+": "+event.values[i]);
            	}
                vals = event.values.clone();		
            	event.values[0]=-vals[1];
            	event.values[1]=vals[0];
                vals = event.values.clone();		

                SensorManager.getRotationMatrixFromVector(
                        mRotationMatrix , event.values);
            }
        }

        public void onDrawFrame(GL10 gl) {
            // clear screen
            gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

            // set-up modelview matrix
            gl.glMatrixMode(GL10.GL_MODELVIEW);
            gl.glLoadIdentity();
            
            
            //Where camera(eye) is
            if(Win == true){
            float eyeX=0, eyeY=yZoom, eyeZ=zZoom, centerX=0, centerY=0, centerZ=-1, upX=0, upY=2, upZ=0;
    		GLU.gluLookAt(gl, eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);
    		long timeTaken=System.nanoTime()-startTime;
    		timeTaken/=1000;
    		if(timeTaken>(5*mul*zZoom)){
    			mul/=1.1;
    			zZoom++;
    			if(zZoom%20==0)
    			yZoom++;

    		}
            }
            
            gl.glTranslatef(0, 0, -3.0f);
            gl.glMultMatrixf(mRotationMatrix, 0);
            for(int i=0, n=mRotationMatrix.length; i<n; i++){
            	//Log.d("myInfo", i+"="+mRotationMatrix[i]);
            }

            // draw our object
            gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
            gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
            
			if(r == 1)
			mpyramid.draw(gl);
			if(r==2)
			mcube.draw(gl);
          
        }

        public void onSurfaceChanged(GL10 gl, int width, int height) {
            // set view-port
            gl.glViewport(0, 0, width, height);
            // set projection matrix
            float ratio = (float) width / height;
            gl.glMatrixMode(GL10.GL_PROJECTION);
            gl.glLoadIdentity();
            
			
            gl.glFrustumf(-ratio, ratio, -1, 1, 1, 70);
        }

        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        	/*
            // dither is enabled by default, we don't need it
            gl.glDisable(GL10.GL_DITHER);
            // clear screen in white
            //gl.glClearColor(1,1,1,1);
            
            
            float red=1.0f, green=1f, blue=1f, alpha=0.0f;
    		float depth=0.0f;
    		gl.glClearColor(red, green, blue, alpha);
    		gl.glClearDepthf(depth);
    		*/
        	gl.glDisable(GL10.GL_DITHER);
        	/*
            gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT,
                    GL10.GL_FASTEST);

             gl.glClearColor(0,0,0,0);
             gl.glEnable(GL10.GL_CULL_FACE);
             gl.glShadeModel(GL10.GL_SMOOTH);
             gl.glEnable(GL10.GL_DEPTH_TEST);
             */
        }

        class pyramid {
            // initialize our pyramid
            private FloatBuffer mVertexBuffer;
            private FloatBuffer mColorBuffer;
            private ByteBuffer  mIndexBuffer;

            public pyramid() {

                final float vertices[] = {
                		//Bottom 4
                		//0				//1
                        -1, -1, -1,      1, -1, -1,
                        //2				//3
                         1,  1, -1,     -1,  1, -1,
                         //Top 4
                         //4			//5
                         0,  0,  1,      0,  0,  1,
                         //6			//7
                         0,  0,  1,      0,  0,  1,

                };


                final float colors[] = {//rgb binary values
                		//Bottom 4
                        0,  0,  1,  1,		  1,  0,  0,  1,
                        1,  0,  0,  1,		  0,  0,  1,  1,
                        //Top 4
                        0,  0,  1,  1,		  1,  0,  0,  1,
                        1,  0,  0,  1,		  0,  0,  1,  1,
                };


                final byte indices[] = {
                		//Right
                        0, 4, 5,    0, 5, 1,
                        //Anti clock = back
                        1, 5, 6,    1, 6, 2,
                        //Anti = left
                        2, 6, 7,    2, 7, 3,
                        //Front
                        3, 7, 4,    3, 4, 0,
                        //Top
                        4, 7, 6,    4, 6, 5,
                        //Bottom
                        //Half		Other half right triangle
                        3, 0, 1,    3, 1, 2
                };


                ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length*4);
                vbb.order(ByteOrder.nativeOrder());
                mVertexBuffer = vbb.asFloatBuffer();
                mVertexBuffer.put(vertices);
                mVertexBuffer.position(0);

                ByteBuffer cbb = ByteBuffer.allocateDirect(colors.length*4);
                cbb.order(ByteOrder.nativeOrder());
                mColorBuffer = cbb.asFloatBuffer();
                mColorBuffer.put(colors);
                mColorBuffer.position(0);

                mIndexBuffer = ByteBuffer.allocateDirect(indices.length);
                mIndexBuffer.put(indices);
                mIndexBuffer.position(0);
            }
         
            public void draw(GL10 gl) {
                gl.glEnable(GL10.GL_CULL_FACE);
                gl.glFrontFace(GL10.GL_CW);
                gl.glShadeModel(GL10.GL_SMOOTH);
                gl.glVertexPointer(3, GL10.GL_FLOAT, 0, mVertexBuffer);
                gl.glColorPointer(4, GL10.GL_FLOAT, 0, mColorBuffer);
                gl.glDrawElements(GL10.GL_TRIANGLES, 36, GL10.GL_UNSIGNED_BYTE, mIndexBuffer);
            }            
        }
        class Cube {
            // initialize our cube
            private FloatBuffer mVertexBuffer;
            private FloatBuffer mColorBuffer;
            private ByteBuffer  mIndexBuffer;

            public Cube() {
                final float vertices[] = {
                        -1, -1, -1,      1, -1, -1,
                         1,  1, -1,     -1,  1, -1,
                        -1, -1,  1,      1, -1,  1,
                         1,  1,  1,     -1,  1,  1,
                };

                final float colors[] = {//rgb binary values
                		//Bottom 4
                        0,  0,  1,  1,		  0,  1,  0,  1,
                        0,  1,  1,  1,		  1,  0,  0,  1,
                        //Top 4
                        1,  0,  1,  1,		  1,  0,  1,  1,
                        1,  0,  1,  1,		  1,  0,  1,  1,
                };


                final byte indices[] = {
                		//Right
                        0, 4, 5,    0, 5, 1,
                        //Anti clock = back
                        1, 5, 6,    1, 6, 2,
                        //Anti = left
                        2, 6, 7,    2, 7, 3,
                        //Front
                        3, 7, 4,    3, 4, 0,
                        //Top
                        4, 7, 6,    4, 6, 5,
                        //Bottom
                        //Half		Other half right triangle
                        3, 0, 1,    3, 1, 2
                };


                ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length*4);
                vbb.order(ByteOrder.nativeOrder());
                mVertexBuffer = vbb.asFloatBuffer();
                mVertexBuffer.put(vertices);
                mVertexBuffer.position(0);

                ByteBuffer cbb = ByteBuffer.allocateDirect(colors.length*4);
                cbb.order(ByteOrder.nativeOrder());
                mColorBuffer = cbb.asFloatBuffer();
                mColorBuffer.put(colors);
                mColorBuffer.position(0);

                mIndexBuffer = ByteBuffer.allocateDirect(indices.length);
                mIndexBuffer.put(indices);
                mIndexBuffer.position(0);
            }

            public void draw(GL10 gl) {
                gl.glEnable(GL10.GL_CULL_FACE);
                gl.glFrontFace(GL10.GL_CW);
                gl.glShadeModel(GL10.GL_SMOOTH);
                gl.glVertexPointer(3, GL10.GL_FLOAT, 0, mVertexBuffer);
                gl.glColorPointer(4, GL10.GL_FLOAT, 0, mColorBuffer);
                gl.glDrawElements(GL10.GL_TRIANGLES, 36, GL10.GL_UNSIGNED_BYTE, mIndexBuffer);
            }            
        }
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    }
}